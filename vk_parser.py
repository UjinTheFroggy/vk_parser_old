# from bs4 import BeautifulSoup
# import requests
# import re
import vk
import os
import requests
import json
import subprocess
from config import VK_TOKEN, COOKIE, PAYLOAD, POST_OPTIONS, OWNER_ID, PROXY


def parse():
    parse_result = {}
    session = vk.Session(access_token=VK_TOKEN)
    api = vk.API(session)
    last_posts = api.wall.get(owner_id=OWNER_ID,
                              count=4, v='5.80')['items']
    for post in last_posts:
        if post['id'] in os.listdir('posts'):
            break
        directory = 'posts/'+str(post['id'])
        post_result = {}
        if not os.path.exists(directory):
            os.makedirs(directory)
            DownloadPicture(post, directory)
            post_result['audio'] = DownloadTrack(post, directory)
            post_result['text'] = post['text']
            parse_result[post['id']] = post_result
    print(parse_result)
    return parse_result


def DownloadPicture(post, directory):
    img = requests.get(post['attachments'][0]
                       ['photo']['sizes'][-1]['url'])
    with open(directory+"/img.jpg", "wb") as handler:
        handler.write(img.content)


def DownloadTrack(post, directory):
    audio_stuff = post['attachments'][1]['audio']
    PAYLOAD['ids'] = str(audio_stuff['owner_id']) + \
        '_' + str(audio_stuff['id'])+','
    print(PAYLOAD)
    resp = requests.post('https://vk.com/al_audio.php', cookies=COOKIE,
                         data=PAYLOAD, params=POST_OPTIONS, proxies=PROXY)
    resp_list = resp.text.split('<!>')
    print(resp_list)
    audio_source = json.loads(resp_list[5][7:])[0][2]
    proc = subprocess.run(
        ['cmd', '/c', 'node', 'decode.js', audio_source], stdout=subprocess.PIPE)
    audio = requests.get(str(proc.stdout)[2:-3])
    track_name = audio_stuff['artist'] + ' - ' + audio_stuff['title'] + '.mp3'
    with open(directory+'/' + track_name, 'wb') as handler:
        handler.write(audio.content)
    return track_name
