import logging
from telegram.ext import Updater, CommandHandler
from config import TOKEN, REQUEST_KWARGS
import os
import shutil
import vk_parser
import time  # временное решение

updater = Updater(TOKEN)#, request_kwargs=REQUEST_KWARGS)
dispatcher = updater.dispatcher

if not 'posts' in os.listdir():
    os.makedirs('posts')

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)


def start(bot, update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text='ЗДАРОВА'
    )


def hello(bot, update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text='ЗДАРОВА НАСТЮХА'
    )


def getPosts(bot, update):
    new_posts = vk_parser.parse()
    for post in sorted(new_posts.items()):
        path = 'posts/'+str(post[0])+'/'
        bot.send_photo(
            chat_id=update.message.chat_id,
            caption=post[1]['text'],
            photo=open(path+'img.jpg', 'rb')
        )
        bot.send_audio(
            chat_id=update.message.chat_id,
            audio=open(path+post[1]['audio'], 'rb')
        )
        time.sleep(1)
    folders = os.listdir('posts')
    i = 0
    while(len(folders) > 1):
        shutil.rmtree('posts/'+folders.pop(i))
        i += 1


# handlers


start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

hello_handler = CommandHandler('hello', hello)
dispatcher.add_handler(hello_handler)

getPosts_handler = CommandHandler('getposts', getPosts)
dispatcher.add_handler(getPosts_handler)

updater.start_polling()
