import json

REQUEST_KWARGS = {'proxy_url': '<Proxy address>'}

PROXY = {'http': '<http proxy address>'}

TOKEN = '<TELEGRAM TOKEN>'

VK_TOKEN = '<VK APP TOKEN>'

COOKIE = {'remixsid': '<GET THIS COOKIE FROM REQUEST TO VK>'}

USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 YaBrowser/18.6.1.770 Yowser/2.5 Safari/537.36'

OWNER_ID = '-<Your VK ID>'

PAYLOAD = {
    'al': 1,
    'act': 'reload_audio',
    'ids': ''
}

POST_OPTIONS = {
    'host': 'vk.com',
    'scheme': 'https',
    'port': '443',
    'path': '/al_audio.php',
    'method': 'POST',
    'headers': {
        'Content-Type': 'application/x-www-form-urlencoded',
        'origin': 'https://vk.com',
        'Content-Length': len(' '.join(format(ord(letter), 'b') for letter in json.dumps(PAYLOAD))),
        'Cookie': COOKIE,
        'referer': '<Public>',
        'user-agent': USER_AGENT,
        'x-requested-with': 'XMLHttpRequest'
    }
}